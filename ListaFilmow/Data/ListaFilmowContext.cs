﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ListaFilmow.Models
{
    public class ListaFilmowContext : DbContext
    {
        public ListaFilmowContext (DbContextOptions<ListaFilmowContext> options)
            : base(options)
        {
        }

        public DbSet<ListaFilmow.Models.Movie> Movie { get; set; }
    }
}
