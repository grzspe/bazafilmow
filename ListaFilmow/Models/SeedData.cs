﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ListaFilmow.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ListaFilmowContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ListaFilmowContext>>()))
            {
                if (context.Movie.Any())
                {
                    return;   
                }

                context.Movie.AddRange(
                    new Movie
                    {
                        Title = "aaa",
                        ReleaseDate = DateTime.Parse("2019-5-12"),
                        Genre = "Komedia romantyczna",
                        Price = 2.99M
                    },

                    new Movie
                    {
                        Title = "bbbb",
                        ReleaseDate = DateTime.Parse("2019-5-13"),
                        Genre = "Komedia",
                        Price = 8.99M
                    },

                    new Movie
                    {
                        Title = "ccc",
                        ReleaseDate = DateTime.Parse("2016-5-14"),
                        Genre = "Dramat",
                        Price = 9.99M
                    },

                    new Movie
                    {
                        Title = "ddd",
                        ReleaseDate = DateTime.Parse("2019-5-15"),
                        Genre = "Western",
                        Price = 3.99M
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
