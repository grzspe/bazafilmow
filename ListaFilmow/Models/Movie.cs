﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;

namespace ListaFilmow.Models
{
    public class Movie
    {
        public int ID { get; set; }
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        [Display(Name = " Data wydania")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        [Display(Name = "Gatunek")]
        public string Genre { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        [Display(Name = "Koszt produkcji w mln $")]
        public decimal Price { get; set; }

    }
}
